class Zombie < ApplicationRecord
	has_many :brains
  belongs_to :user
  mount_uploader :avatar, ImageUploader
	validates:bio, length: { maximum: 100,
    	too_long: "%{count} caracteres es lo maximo aceptable" }
    validates:name,presence:true
    validates:age,numericality:{only_integer:true,message:"Solo numeros enteros"}
    validates:email,format:{with:/\A([^@\s]+)@((?:[-a-z-0-9]+\.)+[a-z]{2,})\z/i, on: :create}
end