Rails.application.routes.draw do
  root 'zombies#index'
  devise_for :users, controllers:{
    sessions: 'users/sessions'
  }
  resources :zombies do
  	resources :brains
  end

  get '/cerebros',to:'brains#index',as:'brains'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
